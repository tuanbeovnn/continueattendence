package fi.vamk.e1800958.attendance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SemdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SemdemoApplication.class, args);
	}

}
